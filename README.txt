 - Instruction: 
Please follow all bellow steps to run on local host:

composer update
rename .env.example with .env (cp .env.example .env)
configure database on .env file
create a new database with name employees_db
create new tables with: php artisan migrate
create default user for system with: php artisan db:seed

(php artisan db:seed --class=PermissionTableSeeder
php artisan db:seed --class=CreateAdminUserSeeder
php artisan db:seed --class=CountriesTableSeeder)

php artisan key:generate
Execute: php artisan config:clear
Execute: php artisan serve
default usename: admin@gmail.com
password:123456

-To run on server-
---------------------------------
php artisan migrate

php artisan db:seed --class=PermissionTableSeeder

php artisan db:seed --class=CreateAdminUserSeeder

php artisan db:seed --class=CountriesTableSeeder