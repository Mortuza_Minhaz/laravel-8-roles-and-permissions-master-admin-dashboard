@extends('layouts.app')

@section('title', 'Users List')


@section('js')


@endsection
@section('css')
<style>
    .portlet.box .dataTables_wrapper .dt-buttons {
        margin-top: 0px;
    }

    .dataTables_wrapper .dt-buttons {
        float: left;
    }

    div.dataTables_wrapper div.dataTables_paginate {
        /* margin: 0; */
        white-space: nowrap;
        /* text-align: right; */
        float: right !important;
    }

    .input-group-sm>.input-group-btn>select.btn,
    .input-group-sm>select.form-control,
    .input-group-sm>select.input-group-addon,
    select.input-sm {
        height: 31px;
        line-height: 30px;
    }
</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">


            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">

                    <div class="dt-buttons" style="margin-top: 5px;">
                        <a class="dt-button buttons-print btn default" tabindex="0" aria-controls="example" href="{{ route('users.create') }}"><span> <i class="fa fa-plus"></i> Create New User</span>

                        </a>
                    </div>

                </div>


                <div class="portlet-body">

                    <table class="table table-striped table-bordered table-hover" id="example">

                        <thead>

                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th width="280px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($data as $key => $item)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('users.show',$item->id) }}">Show</a>
                                  
                                    <a class="btn btn-primary" href="{{ route('users.edit',$item->id) }}">Edit</a>
                                    
                                    
                                    {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $item->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>
{{--{!! $data->render() !!}--}}
<!-- END CONTENT BODY -->
@endsection