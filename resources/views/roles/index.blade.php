@extends('layouts.app')

@section('title', 'Role List')


@section('js')

<script>
     $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            
        ]
    } );
} );
</script>


@endsection

@section('css')

<style>
    .portlet.box .dataTables_wrapper .dt-buttons {
            margin-top: 0px;
            margin-bottom: 20px;
        }

        .dataTables_wrapper .dt-buttons {
            float: left;
        }

        div.dataTables_wrapper div.dataTables_paginate {
            /* margin: 0; */
            white-space: nowrap;
            /* text-align: right; */
            float: right !important;
        }

        .input-group-sm > .input-group-btn > select.btn, .input-group-sm > select.form-control, .input-group-sm > select.input-group-addon, select.input-sm {
            height: 31px;
            line-height: 30px;
        }
  
</style>


@endsection




@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">


            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">

                     <div class="dt-buttons" style="margin-top: 5px;">
                        <a class="dt-button buttons-print btn default" tabindex="0" aria-controls="example" href="{{ route('roles.create') }}"><span> <i class="fa fa-plus"></i> Create New Role</span>

                        </a>
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th width="280px">Action</th>
                            </tr>
                        </thead>
                        <tbody>


                            @foreach ($roles as $key => $role)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $role->name }}</td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                                    @can('role-edit')
                                    <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                                    @endcan
                                    @can('role-delete')
                                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>

<!-- END CONTENT BODY -->
@endsection